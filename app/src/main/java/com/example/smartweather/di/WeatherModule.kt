package com.example.smartweather.di

import com.example.smartweather.ui.adapters.WeatherDayAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object WeatherModule {

    @Singleton
    @Provides
    fun providesDayItemAdapter(): WeatherDayAdapter =
        WeatherDayAdapter()
}