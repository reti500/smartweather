package com.example.smartweather.ui.weather

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.smartweather.data.models.Day
import com.example.smartweather.data.models.FeelsLike
import com.example.smartweather.data.models.Temp
import com.example.smartweather.data.repository.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository
) : ViewModel() {

    private val weatherDaysLiveData = MutableLiveData<List<Day>>()
    val weatherDays : LiveData<List<Day>> = weatherDaysLiveData

    init {
        loadDays()
    }

    private fun loadDays() {
        viewModelScope.launch {
            val result = weatherRepository.getWeatherSevenDays(
                lat = 19.4440215,
                lon = -99.1566862
            )

            if (result.isSuccessful && result.body() != null) {
                weatherDaysLiveData.value = result.body()!!.daily
            } else {
                Log.e("Service", "Error: ${result.errorBody().toString()}")
                weatherDaysLiveData.value = listOf(
                    Day(
                        0,
                        0,
                        0,
                        0,
                        0,
                        0.0,
                        Temp(
                            0.0,
                            0.0,
                            0.0,
                            0.0,
                            0.0,
                            0.0
                        ),
                        FeelsLike(
                            0.0,
                            0.0,
                            0.0,
                            0.0
                        ),
                        0,
                        0,
                        0.0,
                        0.0,
                        0,
                        0.0,
                        listOf(),
                        0,
                        0.0,
                        0.0
                    )
                )
            }
        }
    }
}