package com.example.smartweather.ui.bindings

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.smartweather.R

@BindingAdapter("weatherIcon")
fun setWeatherIcon(view: ImageView, icon: String) {
    view.setImageResource(when(icon) {
        "01d" -> R.drawable.sun
        else -> R.drawable.sun
    })
}

@BindingAdapter("weatherTemp")
fun setWeatherTemp(view: TextView, temp: Double) {
    view.text = "%.2f".format(temp - 273.15) + 'º'
}
