package com.example.smartweather.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.smartweather.R
import com.example.smartweather.data.models.Day
import com.example.smartweather.databinding.ItemDayBinding
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class WeatherDayAdapter @Inject constructor(
) : RecyclerView.Adapter<WeatherDayAdapter.ViewHolder>() {
    var days : List<Day> = emptyList()
    var onItemClick: (day: Day) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ItemDayBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(days[position])

    override fun getItemCount() = days.size

    fun setItems(items: List<Day>) {
        days = items
        notifyDataSetChanged()
    }

    fun setOnDayClick(click: (day: Day) -> Unit) {
        onItemClick = click
    }

    inner class ViewHolder(
        private val binding: ItemDayBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(dayModel: Day) {
            val d = Calendar.getInstance().apply {
                time = Date()
                add(Calendar.DATE, 1)
            }

            binding.apply {
                day = dayModel
                now = SimpleDateFormat("MMMM-dd").format(d.time)

                itemContainer.also {
                    it.setOnClickListener { onItemClick(dayModel) }
                }
            }
        }
    }
}