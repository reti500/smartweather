package com.example.smartweather.ui.splash

import android.animation.Animator
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.example.smartweather.R
import com.example.smartweather.databinding.FragmentSplashBinding

class SplashFragment : Fragment(R.layout.fragment_splash) {

    private lateinit var binding: FragmentSplashBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentSplashBinding.bind(view)

        binding.splashAnimation.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(p0: Animator?) {}

            override fun onAnimationEnd(p0: Animator?) {
                goToWeather()
            }

            override fun onAnimationCancel(p0: Animator?) {
                goToWeather()
            }

            override fun onAnimationRepeat(p0: Animator?) {}

        })
    }

    private fun goToWeather() {
        val navOptions = NavOptions.Builder().setPopUpTo(R.id.weatherFragment, true).build()
        findNavController().navigate(
            R.id.action_splashFragment_to_weatherFragment,
            null,
            navOptions)
    }
}