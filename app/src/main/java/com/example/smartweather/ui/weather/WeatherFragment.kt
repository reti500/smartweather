package com.example.smartweather.ui.weather

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartweather.R
import com.example.smartweather.data.models.Day
import com.example.smartweather.databinding.FragmentWeatherBinding
import com.example.smartweather.ui.adapters.WeatherDayAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class WeatherFragment : Fragment(R.layout.fragment_weather) {

    private val weatherViewModel: WeatherViewModel by viewModels()
    private lateinit var binding: FragmentWeatherBinding

    @Inject
    lateinit var adapter: WeatherDayAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentWeatherBinding.bind(view)

        setupUI()
        observe()
    }

    private fun setupUI() {
        binding.recyclerDays.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerDays.adapter = adapter
    }

    private fun observe() {
        weatherViewModel.weatherDays.observe(viewLifecycleOwner) {
            Log.e("Observer", "Data -> $it")
            adapter.setItems(it)
            adapter.setOnDayClick { day -> goToWeatherDetail(day) }
        }
    }

    private fun goToWeatherDetail(day: Day) =
        findNavController().navigate(
            WeatherFragmentDirections.actionWeatherFragmentToWeatherDetailFragment(
                day
            )
        )
}