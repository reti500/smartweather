package com.example.smartweather.data.api.results

import com.example.smartweather.data.models.Day
import com.google.gson.annotations.SerializedName

data class OneCallResult(
    @SerializedName("lat")
    val lat: Double,

    @SerializedName("lon")
    val lon: Double,

    @SerializedName("timezone")
    val timeZone: String,

    @SerializedName("timezone_offset")
    val timezoneOffset: String,

    @SerializedName("daily")
    val daily: List<Day>
)
