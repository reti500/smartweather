package com.example.smartweather.data.api.services

import com.example.smartweather.data.api.Constants
import com.example.smartweather.data.api.results.OneCallResult
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface OneCallService {

    @GET("onecall")
    suspend fun oneCall(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") apiKey: String = Constants.API_KEY,
        @Query("lang") lang: String = Constants.API_LANG
    ): Response<OneCallResult>

}