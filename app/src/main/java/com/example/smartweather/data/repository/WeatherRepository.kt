package com.example.smartweather.data.repository

import com.example.smartweather.data.api.services.OneCallService

class WeatherRepository(
    private val oneCallService: OneCallService
) {
    suspend fun getWeatherSevenDays(
        lat: Double,
        lon: Double
    ) = oneCallService.oneCall(lat, lon)
}